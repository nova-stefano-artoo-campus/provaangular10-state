angular.module("app").config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.when("/", "/home");
  $urlRouterProvider.otherwise("/");
  
  $stateProvider.state("home", {
    templateUrl: "home/home.template.html",
    url: "/home",
    controller: "homeCtrl",
    data: {
      titolo: "Questa è la home page"
    }
  }).state("home.dettaglio", {
    templateUrl: "home/dettaglio.template.html",
    url: "/dettaglio:id",
    controller: "dettaglioCtrl"
  }).state("contatti", {
    templateUrl: "contatti/contatti.template.html",
    url: "/contatti",
    controller: "contattiCtrl",
    data: {
      titolo: "Questa è la pagina dei contatti"
    },
    resolve: {
      contatti: function(contattiSrv) {
        return contattiSrv.getContatti();
      }
    }
  }).state("chisiamo", {
    abstract: true,
    templateUrl: "chisiamo/chisiamo.template.html",
    url: "/chisiamo",
    controller: "chisiamoCtrl",
    data: {
      titolo: "Questa è la pagina 'Chi siamo'"
    }
  }).state("chisiamo.about", {
    templateUrl: "chisiamo/chisiamo.about.html",
    url: "/about"
  }).state("chisiamo.mission", {
    templateUrl: "chisiamo/chisiamo.mission.html",
    url: "/mission"
  });
});
