angular.module("app").service("contattiSrv", function() {
  var contatti = {
    citta: "Ancona",
    via: "Tal dei tali",
    nazione: "Italia"
  };

  var getContatti = function() {
    return contatti;
  };

  return {
    getContatti: getContatti
  }
});
