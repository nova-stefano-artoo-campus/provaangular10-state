angular.module("app").controller("homeCtrl", function($scope, $state) {
  $scope.titolo = $state.current.data.titolo;

  $scope.listaAmici = [{
    id: 0,
    nome: "Stefano"
  }, {
    id: 1,
    nome: "Daniele"
  }, {
    id: 2,
    nome: "Simone"
  }];
});
